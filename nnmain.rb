x = [1.0, 0.25, -0.5]
h_w = [[rand(-1.0..1.0), rand(-1.0..1.0)],
        [rand(-1.0..1.0), rand(-1.0..1.0)],
        [rand(-1.0..1.0), rand(-1.0..1.0)]]
y_w = [[rand(-1.0..1.0), rand(-1.0..1.0), rand(-1.0..1.0)],
        [rand(-1.0..1.0), rand(-1.0..1.0), rand(-1.0..1.0)]]
desired_ouput = [1.0, -1.0, 0]

def array_multiplication(x, y)
    arr = []
    for i in 0..y[0].length-1
        row = []
        for j in 0..x.length-1
            row << x[j] * y[j][i]
        end
        suma = 0.0
        row.each do |r|
            suma += r
        end
        arr << suma
    end
    return arr
end

def activate(x)
    output = []
    x.each do |v|
        output << Math.tanh(v)
        ##(Math.exp(v) - Math.exp(-1 * v)) / (Math.exp(v) + Math.exp(-1 * v)).round(2)
    end
    return output
end

def derive_error(output, desired_ouput)
    error = []
    for i in 0..output.length-1
        error << 2 * (output[i]-desired_ouput[i])
    end
    return error
end

def derive_output(output)
    derivatives = []
    output.each do |o| 
        derivatives << 1 - (Math.tanh(o) ** 2)
    end
    return derivatives
end

def get_deltas(error, a_y)
    deltas = []
    for i in 0..error.length-1
        deltas << error[i] * a_y[i]
    end
    return deltas
end

def get_new_weights_ol(deltas, weights, ouputs_h)
    eta = 0.01
    new_weights = []
    for i in 0..weights.length-1
        row = []
        for j in 0..deltas.length-1
            row << weights[i][j] - ouputs_h[i] * deltas[j] * eta
        end
        new_weights << row
    end
    return new_weights
end

i = 1
for i in 1..10000
    z_h = array_multiplication(x, h_w)
    a_h = activate(z_h)
    z_y = array_multiplication(a_h, y_w)
    a_y = activate(z_y)

    if a_y == desired_ouput
        puts a_y
        puts "Veces repetido: #{i}"
        break
    end

    error_y = derive_error(a_y, desired_ouput)
    delta_y = get_deltas(error_y, derive_output(a_y))
    old_yw = y_w
    y_w = get_new_weights_ol(delta_y, y_w, a_h)
    delta_h =  get_deltas(array_multiplication(delta_y, old_yw.transpose()), derive_output(a_h))
    h_w = get_new_weights_ol(delta_h, h_w, x)
end

puts a_y
puts "Veces repetido: #{i}"