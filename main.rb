require_relative "nn2"

input_l = Layer.new(3,1)
hidden_l = Layer.new(2,3)
output_l = Layer.new(3,2)
desired_output = [1.0, -1.0, 0]

# Introducción de valores
input_l.neurons[0].inputs << 1.0
input_l.neurons[1].inputs << 0.25
input_l.neurons[2].inputs << -0.5

# Inicialización de pesos
hidden_l.init_rand_ws()
output_l.init_rand_ws()

# Activación capa oculta
hidden_l.neurons.each do |hn|
    input_l.neurons.each do |inn|
        hn.inputs.concat(inn.inputs)
    end
end

hidden_l.act_neurons

# Activación capa salida

output_l.neurons.each do |on|
    hidden_l.neurons.each do |hn|
        on.inputs << hn.output
    end
end

output_l.act_neurons
output_l.show_output

def calc_error(layer, desired_output)
    for i in 0..layer.neurons.length-1
        layer.neurons[i].error = (layer.neurons[i].output - desired_output[i]) ** 2
    end
end

# Inicio del ciclo empezando por el backpropagation
for i in 1..1000

    # Calculo de error de la capa de salida
    for i in 0..output_l.neurons.length-1
        output_l.neurons[i].error = (output.neurons[i].output * desired_output[i]) ** 2
    end

    error = 0.0

    output_l.neurons.each do |on|
        error += on.error
    end

    # Reasignación de los nuevos pesos de la capa de salida
    output_l.neurons.each do |on|
        i = 0
        on.weights.each do |w|
            w = 2 * (on.output - desired_output[i]) * on.output * (1 - on.output) * w.inputs[i]
            i += 1
        end
    end

    # Calculo de error de la capa escondida
    for i in 0..hidden_l.neurons.length-1
        hidden_l.neurons[i].error = (output.neurons[i].output * desired_output[i]) ** 2
    end

    h_error = 0.0

    hidden_l.neurons.each do |hn|
        h_error += hn.error
    end

    #Reasignación de error de la capa escondida
end 