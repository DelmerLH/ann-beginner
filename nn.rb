class InputNeuron
    attr_accessor :value, :output1, :output2
    
    def initialize(value, inweight, outweight1, outweight2)
        @value = value * inweight
        @output1 = @value * outweight1
        @output2 = @value * outweight2
    end
end

class OutputNeuron
    attr_accessor :value, :output

    def initialize(value1, value2, outweight)
        @value = value1 + value2
        @output = @value * outweight
    end
end

def start_nn(val1, val2, weight1, weight2, weight3, weight4)
    in1 = InputNeuron.new(val1, weight1, weight2, weight3)
    in2 = InputNeuron.new(val2, weight1, weight2, weight3)

    on1 = OutputNeuron.new(in1.output1, in2.output1, weight4)
    on2 = OutputNeuron.new(in2.output2, in1.output2, weight4)

    puts on1.output
    puts on2.output
end

inputs = [[1,1], [1,0], [0,1], [0,0], [-1,1], [-1, -1]]

inputs.each do |i|
    puts "outputs for [#{i[0]},#{i[1]}]"
    start_nn(i[0], i[1], 1, 1, 1, 1)
    puts
end

puts "---------------------------------------------------"
puts

inputs.each do |i|
    puts "outputs for [#{i[0]},#{i[1]}]"
    start_nn(i[0], i[1], rand(-0.5..0.5), rand(-0.5..0.5), rand(-0.5..0.5), rand(-0.5..0.5))
    puts
end