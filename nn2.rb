class Neuron
    attr_accessor :inputs, :weights, :output, :error

    # n_conn: numero de conexiones entrantes a la neurona
    def initialize(n_conn)
        @n_conn = n_conn
        @inputs = []
        @weights = []
        @output = 0.0
        @error = 0.0
    end

    # Inicializa aleatoreamente los pesos de la neurona
    def init_rand_w
        for i in 1..@n_conn
            @weights << rand(-1.0..1.0).round(2)
        end
    end

    # Ejecuta la función de activación de la neurona
    def activate(bias)
        if @inputs.empty? && @weights.empty?
            puts "Sin valores"
        else
            for i in 0..@inputs.length-1
                @output += (@inputs[i] * @weights[i]).round(2)
            end 
            @output += bias
            @output = (1 / (1 + Math.exp(-@output))).round(2)
        end
    end

    # Calcula el error de las neuronas
    def calculate_error(desired_output)
        @error = (@output - desired_output) ** 2
    end

    # crea nuevos pesos
    def calc_new_weights(desired_output, is_output_l)
        if (is_output_l == true)
            for i in 0..@inputs - 1
                @weights[i] = 2 * (@output - desired_output) * @output * (1 - @output) * @inputs[i]
            end
        else
            
        end
    end

    def calc_delta(desired_output)
        return 2 * (@output - desired_output) * @output * (1 - @output)
    end
end

class Layer
    attr_accessor :neurons

    # n_neur: numero de neuronas
    # n_conn: numero de conexiones por neurona
    def initialize(n_neur, n_conn)
        @neurons = create_neurons(n_neur, n_conn)
        @bias = rand(0.0..1.0).round(2)
        @error = 0.0
        @delta = 0.0
    end

    # Crea las neuronas y las regresa en un arreglo de neuronas
    def create_neurons(n_neur, n_conn)
        neurons = []
        for i in 1..n_neur
            neurons << Neuron.new(n_conn)
        end
        return neurons
    end

    # Inicializa aleatoreamente los pesos de la neurona
    def init_rand_ws
        @neurons.each do |n|
            n.init_rand_w()
        end
    end

    # Ejecuta la función de activación de la neurona
    def act_neurons
        @neurons.each do |n|
            n.activate(@bias)
        end
    end

    # Muestra la salida de la neurona
    def show_output
        @neurons.each do |n|
            print "#{n.output} - "
        end
    end

    # Calcula el error de la capa
    def calculate_error(desired_output)
        for i in 0..@neurons.length - 1
            @neurons[i].calculate_error(desired_output[i])
            @error += @neurons[i].error
        end
    end

    def calc_delta(desired_output)

    end
end